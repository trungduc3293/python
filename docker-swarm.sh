echo 'deploy app to server prod: ====>'
rm -rf $HOME/pythonapp
mkdir -p $HOME/pythonapp
cd $HOME/pythonapp
git clone https://gitlab.com/trungduc3293/python.git .
sudo docker stack deploy --compose-file docker-compose-prod.yml --with-registry-auth stackpython
echo '=======> deploy success on prod server'